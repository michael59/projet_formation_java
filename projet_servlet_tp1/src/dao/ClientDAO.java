package dao;

import modele.Client;

import java.sql.*;

public class ClientDAO {
    private static String URL = "jdbc:mysql://localhost:3306/client";
    private static String USER = "michael";
    private static String PW = "mdppopmichael";

    private static String SQLfindClientByAlias =
            "SELECT * " +
                    " FROM client " +
                    " WHERE upper(alias) = ? " +
                    " AND password = ?";
    private static String SQLInsertClient=
            "INSERT INTO client VALUES(?,?, ?, ?, ?)";

    private Connection conn = null;

    //singleton
    private static ClientDAO clientDao = null;

    public static ClientDAO getSingleton(){
        if (clientDao == null){
            clientDao = new ClientDAO();
        }
        return clientDao;
    }

    //contructeur privée
    private ClientDAO(){
        try {
            //class for name driver
            conn= DriverManager.getConnection(URL,USER, PW);
        }catch (Exception e){
            System.out.println(e);
        }
    }

    public Client find(String alias, String password) throws Exception{
        if (alias == null || password == null) return null;
        Client client = null;
        PreparedStatement selectStatement = null;
        ResultSet rs = null;

        try{
            selectStatement = conn.prepareStatement(SQLfindClientByAlias);
            selectStatement.setString(1,alias.toUpperCase());
            selectStatement.setString(2,password);

            rs = selectStatement.executeQuery();
            if(rs.next()){
                String nom = rs.getString("nom");
                String prenom = rs.getString("prenom");
                String email = rs.getString("email");

                client = new Client(alias, password, nom, prenom, email);
            }
        }finally {
            rs.close();
            selectStatement.close();
        }
        return client;
    }

    public Client create(String alias, String password, String nom, String prenom, String email) throws Exception{
        if (alias == null || password == null || nom==null || prenom == null || email==null){
            return null;
        }
        Client client = null;
        PreparedStatement insertStatement = null;
        try {
            insertStatement = conn.prepareStatement(SQLInsertClient);
            insertStatement.setString(1, alias.toUpperCase());
            insertStatement.setString(2, password);
            insertStatement.setString(3, nom.toUpperCase());
            insertStatement.setString(4, prenom.toUpperCase());
            insertStatement.setString(5, email.toLowerCase());

            conn.setAutoCommit(false);
            int nb = insertStatement.executeUpdate();
            if(nb==1){
                client = new Client(alias,password, nom, prenom, email);
            }
            conn.commit();
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return client;
    }

    public static void main(String[] args) {
        try {
            ClientDAO dao = ClientDAO.getSingleton();
            Client c1 = dao.find("mic", "tiger");
            System.out.println(c1);
            if (c1 == null){
                Client c2 = dao.create("mic", "tiger", "lol", "mic","lol@lol.lol");
                System.out.println(c2);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
