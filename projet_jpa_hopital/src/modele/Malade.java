package modele;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Malade implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false, length = 30)
    private String nom;
    @Column(nullable = false, length = 30)
    private String prenom;

    @Embedded
    private Adresse adresse = new Adresse(10,"rue des Tulipes", "Lille");

    protected Malade() {

    }
    public Malade(String nom, String prenom, Adresse adresse) {
        this.nom = nom;
        this.prenom = prenom;
        this.adresse = adresse;
    }
}
