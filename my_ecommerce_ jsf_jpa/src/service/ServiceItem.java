package service;


import modele.Item;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.ArrayList;
import java.util.List;

public class ServiceItem {
    //--------------------------------- G E S T I O N   D ' U N   S I N G L E T O N ------------------------------------
    private static ServiceItem serviceItem = new ServiceItem();

    private static EntityManagerFactory emf = JPAUtil.getemf();

    public static ServiceItem getSingleton(){
        return serviceItem;
    }

    //---------------------------------------------------- C R U D -----------------------------------------------------
    public List<Item> findAllItem(){
        List<Item> liste = new ArrayList<>();
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        liste = em.createNamedQuery("Item.findAll", Item.class).getResultList();
        em.getTransaction().commit();
        em.close();
        return liste;
    }

    public Item findItemById(Integer id){
        Item item = new Item();
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        item = em.find(Item.class, id);
        em.getTransaction().commit();
        em.close();
        return item;
    }

}
