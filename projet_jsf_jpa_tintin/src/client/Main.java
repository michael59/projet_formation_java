package client;

import modele.Albums;
import modele.Apparaits;
import service.Service;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        Service service = Service.getSingleton();
        List<Albums> liste = service.findAllAlbums();

        for (Albums a : liste) {
            System.out.println(a);
            for (Apparaits p : a.getApparaits())
                System.out.println("--" + p.getPersonnages());
        }

    }
}
