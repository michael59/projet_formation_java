package util;

import java.io.Serializable;
import java.util.Objects;

public class Achat implements Serializable {
    private int id;
    private int code;
    private int quantite = 1;
    private Double prix;
    private String titre;

    public Achat(int id, String titre, int codeBarre, Double prix, int quantite) {
        this.setCode(codeBarre);
        this.setQuantite(quantite);
        this.setTitre(titre);
        this.setPrix(prix);
        this.setId(id);
    }

    public Double getPrix() {
        return prix;
    }

    public void setPrix(Double prix) {
        this.prix = prix;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCode() {
        return code;
    }
    public void setCode(int code) {
        this.code = code;
    }
    public int getQuantite() {
        return quantite;
    }
    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Achat)) return false;
        Achat achat = (Achat) o;
        return code == achat.code &&
                quantite == achat.quantite;
    }

    @Override
    public int hashCode() {
        return Objects.hash(code);
    }
}
