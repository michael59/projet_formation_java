package modele;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "client")
@NamedQueries({
        @NamedQuery(name="Client.findAll",query = "select c from Client c order by c.nom, c.prenom"),
        @NamedQuery(name="Client.findByNomAndPrenom",query = "select c from Client c where upper(c.nom) = upper(:nom) and" +
                " upper(c.prenom) = upper(:prenom)")
})
public class Client {
    private int id;
    private String nom;
    private String prenom;
    private List<Commande> commandes = new ArrayList<>();
    private List<Payment> payments = new ArrayList<>();

    public Client(String nom, String prenom) {
        this.setNom(nom);
        this.setPrenom(prenom);
    }

    public Client() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nom")
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Basic
    @Column(name = "prenom")
    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return id == client.id &&
                Objects.equals(nom, client.nom) &&
                Objects.equals(prenom, client.prenom);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nom, prenom);
    }

    @OneToMany(mappedBy = "client")
    public List<Commande> getCommandes() {
        return commandes;
    }
    public void setCommandes(List<Commande> commandes) {
        this.commandes = commandes;
    }

    @OneToMany(mappedBy = "client")
    public List<Payment> getPayments() {
        return payments;
    }
    public void setPayments(List<Payment> payments) {
        this.payments = payments;
    }

    @Override
    public String toString() {
        return "Client{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                '}' +"\n";
    }
}
