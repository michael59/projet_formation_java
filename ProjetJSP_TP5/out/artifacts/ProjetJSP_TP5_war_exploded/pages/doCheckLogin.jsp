<%--
  Created by IntelliJ IDEA.
  User: michael
  Date: 19/02/2020
  Time: 16:36
  To change this template use File | Settings | File Templates.
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<jsp:useBean id="login" class="metier.Login" scope="request">
    <jsp:setProperty name="login" property="*"/>
</jsp:useBean>

<%--
    if (login.isConnu()){
%>
<jsp:forward page="afficheView.jsp"/>
<%
    }
--%>
<%--
<jsp:forward page="login.jsp"/>
--%>

<c:if test="${login.connu}">
    <jsp:forward page="afficheView.jsp"/>
</c:if>
<jsp:forward page="login.jsp"/>