package service;

import com.sun.org.apache.xpath.internal.operations.Or;
import modele.Gangster;
import modele.Job;
import modele.Organisation;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.*;

public class Service {

    //Gestion d'un singleton

    private static  Service service = new Service();

    private static EntityManagerFactory emf = JPAUtil.getemf();

    public static Service getSingleton(){
        return service;
    }

    public List<Organisation> findAllOrganisation(){
        List<Organisation> liste = new ArrayList<>();
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        liste = em.createNamedQuery("Organisation.findAll", Organisation.class).getResultList();
        em.getTransaction().commit();
        em.close();
        return liste;
    }

    public void createOrganisation(Organisation org){
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(org);
        em.getTransaction().commit();
        em.close();
    }

    public void createOrganisation(String orgName, String description){
        Organisation org = new Organisation(orgName, description);

        //this.createOrganisation(org); //on pourrait appeler l'autre regle
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(org);
        em.getTransaction().commit();
        em.close();
    }

    public void removeOrganisation(Organisation org){
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        //on recupere la ligne avant de la supprimer sinon il ne sais pas quoi supprimer
        em.remove(em.merge(org));
        em.getTransaction().commit();
        em.close();
    }

    public void updateOrganisation(Organisation org){
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        org = em.merge(org);
        em.getTransaction().commit();
        em.close();

    }
    public List<Gangster> findAllGangster(){
        return emf.createEntityManager().createNamedQuery("Gangster.findAll", Gangster.class).getResultList();
        /* // remplacé par le return
        List<Gangster> liste = new ArrayList<>();
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        liste = em.createNamedQuery("Gangster.findAll", Gangster.class).getResultList();
        em.getTransaction().commit();
        em.close();
        return liste;*/
    }

    public void createGangster(Gangster gang){
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(gang);
        em.getTransaction().commit();
        em.close();
    }


    public void removeGangster(Gangster gang){
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        //on recupere la ligne avant de la supprimer sinon il ne sais pas quoi supprimer
        Gangster gang1 = em.merge(gang);
        gang1.removeOrganisation();
        System.out.println(gang1);
        em.remove(gang1);
        em.getTransaction().commit();
        em.close();
    }

    public void removeGangster(String gName){
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Gangster gangster = em.createNamedQuery("Gangster.findByGname", Gangster.class).setParameter("gname", gName)
                .getSingleResult();

        em.remove(gangster);
        em.getTransaction().commit();
        em.close();
    }

    public Gangster findGangsterByName(String gname){
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Gangster gangster = em.createNamedQuery("Gangster.findByGname", Gangster.class).setParameter("gname", gname)
                .getSingleResult();

        em.getTransaction().commit();
        em.close();
        return gangster;

    }

    public void updateGangster(Gangster gang){
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        gang = em.merge(gang);
        em.getTransaction().commit();
        em.close();
    }

    public void addGangsterToOrganisation(String gname, String orgName){
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Gangster gangster = em.createNamedQuery("Gangster.findByGname", Gangster.class).setParameter("gname", gname)
                .getSingleResult();
        Organisation organisation = em.createNamedQuery("Organisation.findByOrgName", Organisation.class).
                setParameter("orgName", orgName)
                .getSingleResult();
        if (gangster!=null && organisation!=null)
            organisation.addGangster(gangster);

        em.getTransaction().commit();
        em.close();
    }

    public void removeGangsterFromOrganisation(String gName){
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Gangster gangster = em.createNamedQuery("Gangster.findByGname", Gangster.class)
                .setParameter("gname", gName.toUpperCase())
                .getSingleResult();

        gangster.removeOrganisation();
        em.getTransaction().commit();
        em.close();
    }

    public void removeGangsterFromOrganisation(int gangsterId){
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Gangster gangster = em.find(Gangster.class,gangsterId);
        gangster.removeOrganisation();
        em.getTransaction().commit();
        em.close();
    }

    public void addTheBossToORganisation(String gname, String orgName){
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Gangster gangster = em.createNamedQuery("Gangster.findByGname", Gangster.class)
                .setParameter("gname", gname)
                .getSingleResult();
        Organisation organisation = em.createNamedQuery("Organisation.findByOrgName", Organisation.class).
                setParameter("orgName", orgName)
                .getSingleResult();
        if (gangster!=null && organisation!=null)
            organisation.setBoss(gangster);

        em.getTransaction().commit();
        em.close();
    }
    public void addTheBossToORganisation(int gangsterId, String orgName){
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Gangster gangster = em.find(Gangster.class, gangsterId);
        Organisation organisation = em.createNamedQuery("Organisation.findByOrgName", Organisation.class).
                setParameter("orgName", orgName)
                .getSingleResult();
        if (gangster!=null && organisation!=null)
            organisation.setBoss(gangster);

        em.getTransaction().commit();
        em.close();
    }

    public void addJobToGangster(String gName, String jobName){
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Gangster gangster = em.createNamedQuery("Gangster.findByGname", Gangster.class)
                .setParameter("gname", gName)
                .getSingleResult();
        Job job = em.createNamedQuery("Job.findByJobName", Job.class).
                setParameter("jobName", jobName)
                .getSingleResult();

        if (!gangster.getJobs().contains(job)){
            gangster.getJobs().add(job);
            job.getGangsters().add(gangster);
        }


        em.getTransaction().commit();
        em.close();
    }

    public void removeBossOfOrganisation(String orgName){
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Organisation organisation = em.createNamedQuery("Organisation.findByOrgName", Organisation.class).
                setParameter("orgName", orgName)
                .getSingleResult();
        if (organisation.getBoss()!=null){
            organisation.getBoss().setOrganisationGeree(null);
            organisation.setBoss(null);
        }
        em.getTransaction().commit();
        em.close();
    }

    public void addJobToGangster(int gangsterId, String jobName){
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Gangster gangster = em.find(Gangster.class, gangsterId);
        Job job = em.createNamedQuery("Job.findByJobName", Job.class).
                setParameter("jobName", jobName)
                .getSingleResult();

        if (!gangster.getJobs().contains(job)){
            gangster.getJobs().add(job);
            job.getGangsters().add(gangster);
        }

        em.getTransaction().commit();
        em.close();
    }
}
