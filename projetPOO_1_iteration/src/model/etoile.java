package model;

import javafx.css.converter.StringConverter;

public class etoile {
    public static void main(String[] args) {
        for (int i = 1, j = 4; i <= 5; i++, j--) {

            StringBuilder res = new StringBuilder();
            for (int n = 1; n <= j; n++) {
                res.append(" ");
            }
            for (int n = 1; n <= i; n++) {
                res.append("*");
            }
            System.out.println(res);
        }
    }
}

class etoile2 {
    public static void main(String[] args) {
        for (int m = 5, e = 0; m >= 1; m--, e++) {
            StringBuilder res = new StringBuilder();
            res.append("*".repeat(Math.max(0,m)));
            res.append(" ".repeat(Math.max(0,e)));
            System.out.println(res);
        }
    }
}

class etoile3 {
    public static void main(String[] args) {
        int t = 5;
        for (int a = 1; a <= t ; a++) {
            StringBuilder res = new StringBuilder();
            res.append("*".repeat(Math.max(0,a)));
            res.append(" ");
            res.append("*".repeat(Math.max(0,t+1-a)));
            System.out.println(res);
        }
    }
}

class diamand {
    public static void main(String[] args) {
        int t = 7;
        for (int g = ((t-1)/2); g >= -((t-1)/2) ; g--) {
            StringBuilder res = new StringBuilder();
            res.append(" ".repeat(Math.max(0,Math.abs(g))));
            res.append("*".repeat(Math.max(0,(t-2*Math.abs(g)))));
            res.append(" ".repeat(Math.max(0,Math.abs(g))));
            System.out.println(res);
        }
    }
}


class nombrePremier {
    public static void main(String[] args) {
        int m = 1;
        for (int i = 2 ; i <= 100 ; i++){
            int resultat = 0 ; //initialisation du resultat pour voir s'il a changer
            int n = 2; // initialisation du compteur n pour la boucle while
            while (n<i)
            {
                if(i%n == 0) //vérifi s'il n'y a aucun reste si le chiffre est divisible on change la variable resultat
                {
                    resultat=n;
                }
                n++;
            }
            if(resultat == 0) //si la variable résultat n'a pas ete changer alors le chiffre est un nombre premier
            {
                if(m%8 != 0) {
                    System.out.print(i);
                    System.out.print(" ");
                    m++;
                }
                else {
                    System.out.println(i);
                    m++;
                }
            }
        }
    }
}
class eratostene{
    public static void main(String[] args) {
        //initialisation des valeurs demandé et taille du tableau
        int voulu = 70;
        int taille = voulu+1;
        //mise en pase pour l'affichage
        String srt = String.valueOf(voulu);
        int lo = srt.length();
        //construction du tableau
        boolean[] tableau = new boolean[taille];
        //remplissage de toutes les colonnes du tableau
        for (int p=0; p <= voulu ; p++ ) {
            tableau [p] = true;
        }
        //je ne veux pas la valuer zero donc je la mets a false
        tableau[0]= false;
        //je vérifie si les chiffres sont divisible
        for (int i=2;i<=voulu;i++){
            for (int j=i+1; j<=voulu;j++){
                if(j%i==0)
                    tableau[j]= false;
            }
        }
        //initialisation de ma variable pour le compteur d'affichage
        int r=1;
        //affichage des nombre premier
        for (int v=0; v<=voulu; v++) {
            if (tableau[v]){

                if (r%8 != 0){
                    System.out.printf(" %"+lo+"d",v);
                    r++;
                }
                else {
                    System.out.printf(" %"+lo+"d\n", v);
                    r++;
                }
            }
        }
    }
}


