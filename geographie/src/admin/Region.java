package admin;

public class Region extends Zone{

    //attributs d'instance

    public String responssable;

    //getter setter

    public String getResponssable() {
        return responssable;
    }

    public void setResponssable(String president) {
        this.responssable = (president==null ? "PRESIDENT" : president.toUpperCase());
    }

    // constructeur

    public Region(String nom ,String responssable) {
        super(nom);
        this.setResponssable(responssable);
    }

    @Override
    public String toString() {
        return super.toString()+
                "\n{" +
                "president ='" + responssable + '\'' +
                '}';
    }

}
