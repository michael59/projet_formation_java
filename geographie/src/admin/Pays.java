package admin;

public class Pays extends Zone{
    //attributs d'instance

    public String president;

    //getter setter

    public String getPresident() {
        return president;
    }

    public void setPresident(String president) {
        this.president = (president==null ? "PRESIDENT" : president.toUpperCase());
    }

    // constructeur

    public Pays(String nom, String president) {
        super(nom);
        this.setPresident(president);
    }

    //to string

    @Override
    public String toString() {
        return super.toString()+
                "\n{" +
                "president ='" + president + '\'' +
                "population = " + getPopulation() +
                '}';
    }
}
