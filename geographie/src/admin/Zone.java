package admin;

import java.util.*;


public class Zone implements Comparable<Zone>{ //ordre naturel

    //attribut de classe
    private static Map<String, Zone> ensZones = new TreeMap<>();

    //attribut d'instance
    private String nom;

    //association --> mere
    private Zone mere = null;

    //association fille
    private Set<Zone> filles = new TreeSet<>();

    //dehinition ordre naturel : comparaison des noms des zones
    public int compareTo(Zone autre){
        return this.nom.compareTo(autre.nom);
        //return (new Integer(this.getPopulation())).compareTo(new Integer(autre.getPopulation()));

    }

    //Metier
    public int getPopulation(){
        int somme = 0;
        for (Zone f : filles){
            somme = somme + f.getPopulation();
        }
        return somme;
    }

    //getter setter

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    //constructeur

    public Zone(String nom){
        this.setNom(nom);
    }

    //Methode de classe

    public static void ajouterPays(String nom, String president) throws Exception {
        if(nom == null)
            throw new Exception("Zone - création Pays - Le nom est obligatoire");
        String cle = nom.toUpperCase();
        if (Zone.ensZones.containsKey(cle))
            throw new Exception("Zone - création Pays - Le nom doit être unique");
       // president=(president==null?"PRESIDENT": president.toUpperCase());
        Zone nouv = new Pays(cle, president);
        Zone.ensZones.put(cle,nouv);
    }

    public static void ajouterRegion(String nom, String responssable, String nomPays) throws Exception{
        if(nom == null)
            throw new Exception("Zone - création Region - Le nom est obligatoire");
        String cle = nom.toUpperCase();
        if (Zone.ensZones.containsKey(cle))
            throw new Exception("Zone - création Region - Le nom doit être unique");

        if (nomPays==null)
            throw new Exception("Zone - création Region - Le nom du pays ne peut être null");
        String cle2 = nomPays.toUpperCase();

        if (! Zone.ensZones.containsKey(cle2))
            throw new Exception("Zone - création Region - Le pays est inconnu");

        Zone zonePays = Zone.ensZones.get(cle2);
        if(!(zonePays instanceof Pays))
            throw new Exception("Zone - création Region - 1 objet n'est pas un pays");


        Zone zoneRegion = new Region(nom, responssable);
        zoneRegion.mere = zonePays;
        zonePays.filles.add(zoneRegion);

        Zone.ensZones.put(cle,zoneRegion);
    }

    public static void ajouterDepartement(String nom, String prefet, String nomRegion) throws Exception{
        if(nom == null)
            throw new Exception("Zone - création Departement - Le nom est obligatoire");
        String cle = nom.toUpperCase();
        if (Zone.ensZones.containsKey(cle))
            throw new Exception("Zone - création Departement - Le nom doit être unique");

        if (nomRegion==null)
            throw new Exception("Zone - création Departement - Le nom de la region ne peut être null");
        String cle2 = nomRegion.toUpperCase();
        if (! Zone.ensZones.containsKey(cle2))
            throw new Exception("Zone - création Departement - La region est inconnu");

        Zone zoneRegion = Zone.ensZones.get(cle2);
        if(!(zoneRegion instanceof Region))
            throw new Exception("Zone - création Departement - 1 objet n'est pas une region");


        Zone zoneDepartement = new Departement(nom, prefet);
        zoneDepartement.mere = zoneRegion;
        zoneRegion.filles.add(zoneDepartement);

        Zone.ensZones.put(cle,zoneDepartement);
    }

    public static void ajouterCommune(String nom, String maire, String nomDepartement, int population) throws Exception{
        if(nom == null)
            throw new Exception("Zone - création Département - Le nom est obligatoire");
        String cle = nom.toUpperCase();
        if (Zone.ensZones.containsKey(cle))
            throw new Exception("Zone - création Département - Le nom doit être unique");

        if (nomDepartement==null)
            throw new Exception("Zone - création Region - Le nom du departement ne peut être null");
        String cle2 = nomDepartement.toUpperCase();
        if (! Zone.ensZones.containsKey(cle2))
            throw new Exception("Zone - création Region - Le département est inconnu");

        Zone zoneDepartement = Zone.ensZones.get(cle2);
        if(!(zoneDepartement instanceof Departement))
            throw new Exception("Zone - création Region - 1 objet n'est pas un departement");


        Zone zoneCommune = new Commune(nom, maire, population);
        zoneCommune.mere = zoneDepartement;
        zoneDepartement.filles.add(zoneCommune);

        Zone.ensZones.put(cle,zoneCommune);

    }

    //tostring

    @Override
    public String toString() {
        return this.getClass().getSimpleName()+  //permet de recupérer la nature de la zone
                "{"+
                "nom='" + nom + '\'' +
                '}';
    }

    public static Zone rechercher(String nom){
        if(nom==null)
            return null;
        return Zone.ensZones.get(nom.toUpperCase());
    }

    //afficher le recencement
    public static void afficherPop(String nom){
        Zone z = Zone.rechercher(nom);
        if (z==null)
            return;
        z.afficherBis(0);
        System.out.println("");

    }

    private void afficherBis(int decalage){
        for(int i=0; i<decalage;i++)
            System.out.print("\t");
            System.out.println(this.getClass().getSimpleName() +
                    " " + this.getNom() +
                    " ; " + this.getPopulation());
            for (Zone f : this.filles) {
                f.afficherBis(decalage + 1);
            }

    }

    public static void afficherCle(){
        System.out.println("N O M  D E S  Z O N E S");
        for (String cle:Zone.ensZones.keySet()){
            System.out.println(cle);
        }
    }

    public static void main(String[] args)throws Exception {
        try {
            Zone.ajouterPays("FRANCE", "ptitbite");
            Zone f = Zone.rechercher("FRANCE");

            Zone.ajouterRegion("HAUT DE FRANCE", "BOUCHEZ MARC", "FRANCE");

            Zone.ajouterDepartement("NORD", "DEDE BOUFFE", "HAUT DE FRANCE");
            Zone.ajouterDepartement("PAS DE CALAIS", "LEFFE BOUFFE", "HAUT DE FRANCE");

            Zone.ajouterCommune("Monchaux sur ecaillon","Michael lefebvre", "NORD", 645);
            Zone.ajouterCommune("Maing","TITI", "NORD", 1680);
            Zone.ajouterCommune("bouchain","bernard", "NORD", 1056);
            Zone.ajouterCommune("famars","zebre", "NORD", 2058);
            Zone.ajouterCommune("LIEVIN","duff", "PAS DE CALAIS", 12489);

            afficherPop("FRANCE");
            afficherPop("Nord");

            Zone.afficherCle();
        }
        catch (Exception ex){
            System.err.println(ex.getMessage());
        }
    }

}
