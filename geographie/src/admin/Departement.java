package admin;

public class Departement extends Zone{
    //attributs d'instance

    public String prefet;

    //getter setter

    public String getPrefet() {
        return prefet;
    }

    public void setPrefet(String prefet) {
        this.prefet = (prefet==null ? "PREFET" : prefet.toUpperCase());
    }

    // constructeur

    public Departement(String nom ,String prefet) {
        super(nom);
        this.setPrefet(prefet);
    }

    @Override
    public String toString() {
        return super.toString()+
                "\n{" +
                "president ='" + prefet + '\'' +
                '}';
    }
}
