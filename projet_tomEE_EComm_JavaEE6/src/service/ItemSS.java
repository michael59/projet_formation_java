package service;

import modele.Item;

import javax.ejb.Singleton;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.*;

@Singleton
public class ItemSS {
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////// service CRUD : entite Item ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @PersistenceContext(unitName = "ECommPU")
    private EntityManager em;

    public List<Item> findAllItems(){
        return em.createNamedQuery("Item.findAll", Item.class)
                .getResultList();
    }

    public void create(Item item){
        em.persist(item);
    }

    public void update(Item item){
        item = em.merge(item);
    }

    public void delete(Item item){
        item = em.merge(item);
        em.remove(item);
        // ou em.remove(em.merge(item));
    }
}
