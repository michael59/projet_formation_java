package modele;

import javax.persistence.*;
import javax.sql.rowset.serial.SerialArray;
import java.io.Serializable;
import java.util.Objects;

@Entity
public class Participation implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(length = 40)
    private String fonction;

    @ManyToOne
    private Medecin medecin;

    @ManyToOne
    private Equipe equipe;

    public Medecin getMedecin() {
        return medecin;
    }

    public void setMedecin(Medecin medecin) {
        this.medecin = medecin;
    }

    public Equipe getEquipe() {
        return equipe;
    }

    public void setEquipe(Equipe equipe) {
        this.equipe = equipe;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFonction() {
        return fonction;
    }

    public void setFonction(String fonction) {
        this.fonction = fonction;
    }

    public Participation() {
    }

    public Participation(String fonction) {
    setFonction(fonction);
    }

    public Participation(String fonction, Medecin medecin, Equipe equipe){
        setFonction(fonction);
        this.medecin=medecin;
        this.equipe=equipe;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Participation)) return false;
        Participation that = (Participation) o;
        return fonction.equals(that.fonction) &&
                medecin.equals(that.medecin) &&
                equipe.equals(that.equipe);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fonction, medecin, equipe);
    }


}
