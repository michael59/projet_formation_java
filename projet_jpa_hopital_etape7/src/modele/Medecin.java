package modele;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
@DiscriminatorValue(value="Med")
@NamedQueries(
        {
                @NamedQuery(
                        name="Medecin.findNomAndSalaire",
                        query="SELECT new dto.MedInfo(med.nom,med.salaire) " +
                                "from Medecin med " +
                                "WHERE upper(med.service.nom) = :nom "
                ),
                @NamedQuery(name="Medecin.findAll",
                        query = "SELECT med " +
                                "from Medecin med " +
                                " order by med.nom, med.prenom"
                ),
                @NamedQuery(name="Medecin.findAllAvecEquipe",
                        query = "SELECT distinct med " +
                                "from Medecin med join fetch med.participations" +
                                " WHERE med.participations is not empty " +
                                " order by med.nom, med.prenom "
                ),
                @NamedQuery(name="Medecin.findMedecinOfEquipe",
                        query = "SELECT p.medecin " +
                                "from Participation p " +
                                " WHERE p.equipe.nom = :nomEquipe "
                ),
                @NamedQuery(name="Equipe.findEquipeOfMedecin",
                        query = "SELECT p.equipe " +
                                "from Participation p " +
                                " WHERE p.medecin.nom = :nomMedecin " +
                                " and p.medecin.prenom = :prenomMedecin"
                )
        }
)

public class Medecin extends Personne implements Serializable {
    private static final long serialVersion = 1L;

    private float salaire;

    @ManyToOne
    @JoinColumn(name="service_id")
    private Service service;

    @ManyToOne
    @JoinColumn(name = "manager_id")
    private Medecin mgr;

    @OneToMany(mappedBy = "mgr")
    private Set<Medecin> subs = new HashSet<>();

    @OneToMany(mappedBy = "medecin", cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private Set<Participation> participations = new HashSet<>();



    public float getSalaire() {
        return salaire;
    }

    public void setSalaire(float salaire) {
        this.salaire = salaire;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    protected Medecin(){
        super("dupond", "jean");
    }

    public Medecin(String nom, String prenom, float salaire){
        super(nom, prenom);
        this.salaire=salaire;
    }

    public Medecin getMgr() {
        return mgr;
    }

    public Set<Medecin> getSubs() {
        return subs;
    }

    public void setSubs(Set<Medecin> subs) {
        this.subs = subs;
    }

    public void setMgr(Medecin mgr){
        if (this.getMgr() !=null ){
            if (this.getMgr().equals(mgr))
                return;
            else
                this.getMgr().getSubs().remove(this);
        }
        //gestion des deux extremites
        if (mgr!=null) {
            mgr.subs.add(this);
        }
        this.mgr = mgr;
    }
    //definir equipe

    public void ajoutParticipation(Equipe equipe, String fonction){
        if (equipe == null)return;

        Participation participation = new Participation(fonction, this, equipe);


        equipe.getParticipations().add(participation);
        this.getParticipations().add(participation);
    }

        public Set<Participation> getParticipations() {
            return participations;
        }

        public void setParticipations(Set<Participation> participations) {
            this.participations = participations;
        }

    @Override
    public String toString() {
        return super.toString() + '\'' +
                ", salaire=" + salaire +
                ", service=" + (service==null?", sans service" : service.getNom()) +
                ", mgr=" + (mgr==null?"sans manager":mgr.getNom()) +
                '}';
    }
}


