
<%--
  Created by IntelliJ IDEA.
  User: michael
  Date: 20/02/2020
  Time: 14:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" import="beans.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="techSupportBean"
             scope="session"
             type="beans.ControlBean"/>

<hr/>
Utilisateur actuel :
<%-- A la boomer --%>
<jsp:getProperty name="techSupportBean" property="*"/>

<%-- a la millenium --%>
${sessionScope.techSupportBean.lastname}

<hr/>

Entreprise XYZ, Service Client au 1.800.xyz.corp <br>

<c:remove scope="session" var="techSupportBean"/>