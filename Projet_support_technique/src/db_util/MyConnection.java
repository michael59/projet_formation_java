package db_util;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MyConnection {
    //IDENTIFICATION BASE DE DONNEES
    private final static String URL="jdbc:mysql://localhost:3306/support_technique";
    private final static String USER= "michael";
    private final static String PW = "mdppopmichael";

    //SINGLETON : DECLARATION
    private static Connection INSTANCE = null;

    //SINGLETON : INITIALISATION
    static{
        try{
            INSTANCE = DriverManager.getConnection(URL,USER,PW);
        }catch (SQLException e){
            e.printStackTrace();
        }
    }


//SIBGLETON : ACCESSEUR
    public static java.sql.Connection getINSTANCE(){
        return INSTANCE;
    }


}
