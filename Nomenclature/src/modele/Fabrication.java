package modele;

import java.util.Objects;

public class Fabrication {
    //Classe données

    //protected limite l'acces au package
    //attention eviter d'utiliser getter setter
    protected Produit origine = null;
    protected Produit destination = null;
    protected int quantite = 1;
    protected String lieu;

    //getter setter

    //constructeur
    protected Fabrication(int quantite, String lieu, Produit origine, Produit destination) {
        //origine et destination sont testé dans la methode add composant
        this.origine = origine;
        this.destination = destination;
        this.quantite = (quantite <1 ? 1 : quantite);;
        this.lieu = (lieu==null ? "LILLE" : lieu.toUpperCase());
    }

    //EGALITE


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Fabrication that = (Fabrication) o;
        return origine.equals(that.origine) &&
                destination.equals(that.destination);
    }

    @Override
    public int hashCode() {
        return Objects.hash(origine, destination);
    }


}
