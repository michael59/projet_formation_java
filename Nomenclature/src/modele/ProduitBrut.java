package modele;

public class ProduitBrut extends Produit {
    //attributs d'instance
    private double prix;

    //getter

    public double getPrix() {
        return prix;
    }


    //constructeur

    public ProduitBrut(String nom, double prix) {
        super(nom);
        this.prix = prix;
    }

    @Override
    public boolean estCompatible(Produit pr) {
        return false;
    }

    //tostring

    @Override
    public String toString() {
        return super.toString()+"  " +
                "prix=" + prix;
    }
}
