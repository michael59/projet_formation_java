package beans;

import java.io.*;
import java.util.Objects;

public class ProduitBean implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	// Le Data Transfert Object
	private String id;   // cle metier
    private String nom;
    private String descr;
    private float prix;

    public String getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public String getDescr() {
        return descr;
    }

    public float getPrix() {
        return prix;
    }
    
    void setId(String id) {
        this.id = id;
    }

    void setNom(String nom) {
        this.nom = nom;
    }

    void setDescr(String descr) {
        this.descr = descr;
    }

    void setPrix(float prix) {
        this.prix = prix;
    }

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof ProduitBean)) return false;
		ProduitBean that = (ProduitBean) o;
		return id.equals(that.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public String toString() {
		return "ProduitBean [id=" + id + ", nom=" + nom + ", descr=" + descr
				+ ", prix=" + prix + "]";
	}
     
}
