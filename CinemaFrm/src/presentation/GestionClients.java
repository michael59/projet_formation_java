package presentation;

import javax.swing.*;

public class GestionClients {
    private JPanel rootPanel;
    private JTabbedPane tabbedPane1;
    private JComboBox comboClient;
    private JComboBox comboFilm;
    private JButton emprunterButton;


    public static void main(String[] args) {
        JFrame frame = new JFrame("GestionClients");
        frame.setContentPane(new GestionClients().rootPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
