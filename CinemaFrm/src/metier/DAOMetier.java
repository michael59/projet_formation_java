package metier;

import dto.*;
import javafx.collections.ArrayChangeListener;

import java.lang.reflect.Array;
import java.sql.*;
import java.util.*;

public class DAOMetier implements IDAOMetier{

    private final static String SQLfindALLGenres =
            "SELECT ngenre, nature  " +
                    " FROM GENRE ORDER BY nature";

    private final static String SQLfindNbFilmsByGenre =
            "SELECT COUNT(*) as NB " +
                    " from FILM " +
                    " where ngenre = ?";

    private final static String SQLfindAllFilmByGenre =
            "SELECT nfilm , titre, realisateur as NOM_REALISATEUR, CONCAT(nom, ' ',prenom) as NOM_ACTEUR " +
                    " from FILM f " +
                    " INNER JOIN ACTEUR ON nacteur = nacteurPrincipal " +
                    "where f.ngenre = ? " +
                    "ORDER BY titre";

    private final static String SQLfindFilmById =
            "SELECT nfilm , titre, realisateur as NOM_REALISATEUR, nom as NOM_ACTEUR " +
                    " from FILM f " +
                    " INNER JOIN ACTEUR ON nacteur = nacteurPrincipal " +
                    " where f.nfilm = ? " +
                    "ORDER BY titre";

    private final static String SQLfindAllFilmEmpruntables =
            "SELECT nfilm , titre, realisateur as NOM_REALISATEUR, nom as NOM_ACTEUR " +
                    " from FILM f " +
                    " INNER JOIN ACTEUR " +
                    " ON NACTEURPRINCIPAL = NACTEUR " +
                    " where not nfilm in " +
                    " (SELECT nfilm FROM emprunt WHERE retour = 'non') " +
                    " ORDER BY titre";

    private final static String SQLfindAllFilmEmpruntablesByClient =
            "SELECT nfilm , titre, realisateur as NOM_REALISATEUR, nom as NOM_ACTEUR " +
                    " from FILM f " +
                    " INNER JOIN ACTEUR " +
                    " ON NACTEURPRINCIPAL = NACTEUR " +
                    " where not nfilm in " +
                    " (SELECT nfilm FROM emprunt JOIN client using(nclient) WHERE retour = 'non' AND nclient = ? ) " +
                    " ORDER BY titre";

    private final static String SQLcreateClient =
            "INSERT INTO CLIENT(nom, prenom, adresse, anciennete) VALUES ( ? , ? , ? , ? ) ";

    private final static String SQLdeleteClient =
            "DELETE FROM CLIENT WHERE nclient = ? ";

    private final static String SQLupdateClient =
                        "UPDATE CLIENT "
                    +   " SET adresse = ? , "
                    +   " anciennete = ? "
                    +   " WHERE nclient = ? ";

    private final static String SQLfindAllClients =
                    "SELECT nclient, nom, prenom, adresse, anciennete " +
                    " FROM CLIENT " +
                    " ORDER BY nom, prenom";

    private final static String SQLfindAllClientsById =
                    "SELECT nclient, nom, prenom, adresse, anciennete " +
                    " FROM client " +
                    "WHERE nclient = ?";

    //gestion des emprunts ---------- DEUX VERSIONS
    private final static String SQLEmprunterOne =
                "INSERT INTO EMPRUNT(nclient,nfilm, retour, dateEmprunt) "
            +   " values ( "
            +   " (SELECT nclient FROM CLIENT WHERE nom = ? and prenom = ? ), "
            +   " (SELECT nfilm FROM FILM WHERE titre = ? ) , "
            +   " 'non', current_date)";

    private final static String SQLEmprunterTwo =
                        "INSERT INTO EMPRUNT(nclient, nfilm, retour , dateEmprunt) "
                    +   " values ( ?, ?, 'non', current_date) ";

    private final static String SQLRestituer =
            "UPDATE EMPRUNT "
            +   "SET retour = 'oui' "
            +   "WHERE "
            +   " nclient = ? "
            +   " and nfilm = ? ";



    private final static String SQLDeleteEmpruntOfClient =
            "DELETE FROM EMPRUNT "
                    +   " WHERE nclient = ? ";

    private final static String SQLNbreEmpruntEnCoursOfClient =
                " SELECT COUNT(*) as nbr "
            +   " FROM CLIENT "
            +   " INNER JOIN EMPRUNT "
            +   " USING(nclient) "
            +   " WHERE retour = 'non' AND nclient = ? ";

    private final static String SQLEmpruntEnCoursOfClient =
            " SELECT nemprunt"
                    +   " FROM CLIENT "
                    +   " INNER JOIN EMPRUNT "
                    +   " USING(nclient) "
                    +   " WHERE retour = 'non' AND nclient = ? ";

    private final static String SQLDeleteClient =
            "DELETE FROM CLIENT where nclient = ? ";


    //---------------------------------------------------------------------------------------------


    @Override
    public void deleteClient(Client client) {
        deleteClient(client.getCodeClient());
    }

    @Override
    public void deleteClient(int nclient) {
        //List<Integer> liste = new ArrayList<>();

        Connection conn = ConnectCinema.getInstance();

        try {
            conn.setAutoCommit(false);
            PreparedStatement pstmt =
                    conn.prepareStatement(SQLEmpruntEnCoursOfClient);
            ResultSet rs = null;
            pstmt.setInt(1,nclient);
            rs = pstmt.executeQuery();

            if (rs.next()){
                System.out.println("impossible"); //emprunt en cours
                conn.rollback();
            }else {
                System.out.println("possible"); //pas d'emprunt en cours

                PreparedStatement pstmt2 = conn.prepareStatement(SQLDeleteEmpruntOfClient);
                pstmt2.setInt(1,nclient);
                int nb2 = pstmt2.executeUpdate();

                PreparedStatement pstmt3 = conn.prepareStatement(SQLDeleteClient);
                pstmt3.setInt(1,nclient);
                int nb3 = pstmt3.executeUpdate();

               conn.commit();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            try {
                conn.rollback();
            }catch (SQLException ex){
                ex.printStackTrace();
            }

        }
    }

    @Override
    public List<GenreDTO> ensGenres() {
        List<GenreDTO> liste = new ArrayList<>();

        try{
            Statement instr = ConnectCinema.getInstance().createStatement();
            ResultSet rs = instr.executeQuery(SQLfindALLGenres);

            while (rs.next()){
                int ngenre = rs.getInt(1);
                String nature = rs.getString("nature");

                liste.add(new GenreDTO(ngenre, nature));
            }
        }catch (SQLException ex){
            ex.printStackTrace();
        }
        return liste;
    }

    @Override
    public long nbreFilmDuGenre(int numGenre) {
        try{
            PreparedStatement ps =
                    ConnectCinema.getInstance().prepareStatement(SQLfindNbFilmsByGenre);

            ps.setInt(1,numGenre);
            ResultSet rs = ps.executeQuery();
            if (rs.next()){
                long nb = rs.getLong("NB");

                return nb;
            }
        }catch (SQLException ex){
            ex.printStackTrace();
        }
        return -1L;
    }

    @Override
    public List<FilmDTO> ensFilmsDuGenre(int numGenre) {
        List<FilmDTO> liste = new ArrayList<>();
        try{
            PreparedStatement ps = ConnectCinema.getInstance().prepareStatement(SQLfindAllFilmByGenre);

            ps.setInt(1,numGenre);
            ResultSet rs = ps.executeQuery();

            while (rs.next()){
                int nfilm = rs.getInt("nfilm");
                String titre = rs.getString("titre");
                String nomReal = rs.getString(3); //3 correspond a la colonne 3
                String nomActeur = rs.getString(4); //4 correspond a la colonne 4

                FilmDTO f = new FilmDTO(nfilm, titre, nomReal, nomActeur);
                liste.add(f);
            }

        }catch (SQLException ex){
            ex.printStackTrace();
        }
        return liste;
    }

    @Override
    public FilmDTO infoRealisateurEtActeur(int numFilm) {
        FilmDTO f = null;
        try{
            PreparedStatement ps = ConnectCinema.getInstance().prepareStatement(SQLfindFilmById);
            ps.setInt(1,numFilm);
            ResultSet rs = ps.executeQuery();

            if(rs.next()){
                int nfilm = rs.getInt("nfilm");
                String titre = rs.getString("titre");
                String nomReal = rs.getString(3);
                String nomActeur = rs.getString(4);

                f = new FilmDTO(nfilm, titre,nomReal,nomActeur);
            }

        }catch(SQLException ex){
            ex.printStackTrace();
        }
        return f;
    }


    // ---------------------------------------S I N G L E T O N   P R É C O C E -----------------------
    private static  DAOMetier INSTANCE = new DAOMetier();
    private DAOMetier(){}
    public static IDAOMetier getInstance(){
        return INSTANCE;
    }

    //------------------------------------------------------------------------------
    @Override
    public List<ActeurDTO> ensActeurs() {
        List<ActeurDTO> liste = new ArrayList<>();
       /*try{
            PreparedStatement ps = ConnectCinema.getInstance().prepareStatement();

        }catch(SQLException ex){
            ex.printStackTrace();
        }*/
        return liste;
    }

    @Override
    public List<FilmDTO> ensTitreDunActeur(int numActeur) {
        return null;
    }

    @Override
    public List<FilmDTO> ensFilmEmpruntables() {
        return null;
    }

    @Override
    public void createClient(String nom, String prenom, String adresse, long anciennete) {
        //controle divers
        nom= (nom == null ? "GATOR" : nom.toUpperCase());
        prenom = (prenom == null ? "MAGALIE" : prenom.toUpperCase());
        adresse = (adresse == null ? "LILLE" : adresse.toUpperCase());
        anciennete = (anciennete<0?0:anciennete);
        Connection conn = ConnectCinema.getInstance();
        try {
            PreparedStatement ps = conn.prepareStatement(SQLcreateClient);

            ps.setString(1,nom);
            ps.setString(2,prenom);
            ps.setString(3, adresse);
            ps.setLong(4,anciennete);

            conn.setAutoCommit(false);
            ps.executeUpdate();
            conn.commit();
        }catch (SQLException e){
            e.printStackTrace();
            try {
                conn.rollback();
            }catch (SQLException ex){
                ex.printStackTrace();
            }
        }
    }

    @Override
    public List<Client> findAllClients() {
        List<Client> liste = new ArrayList<>();
        try{
            Statement ps = ConnectCinema.getInstance().createStatement();
            ResultSet rs = ps.executeQuery(SQLfindAllClients);

            while (rs.next()){

                liste.add(new Client(rs.getInt(1),
                                        rs.getString(2),
                                        rs.getString(3),
                                        rs.getString(4),
                                        rs.getLong(5)));

            }

        }catch (SQLException ex){
            ex.printStackTrace();
        }
        return liste;
    }

    @Override
    public void emprunter(int nclient, int nfilm) {

        Connection conn = ConnectCinema.getInstance();
        try {
            PreparedStatement ps = conn.prepareStatement(SQLEmprunterTwo);

            ps.setInt(1, nclient);
            ps.setInt(2, nfilm);

            conn.setAutoCommit(false);
            ps.executeUpdate();
            conn.commit();
        }catch (SQLException e){
            e.printStackTrace();
            try {
                conn.rollback();
            }catch (SQLException ex){
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void emprunter(String nom, String prenom, String titre) {

        Connection conn = ConnectCinema.getInstance();
        try {
            PreparedStatement ps = conn.prepareStatement(SQLEmprunterOne);

            ps.setString(1, nom);
            ps.setString(2, prenom);
            ps.setString(3, titre);


            conn.setAutoCommit(false);
            ps.executeUpdate();
            conn.commit();
        }catch (SQLException e){
            e.printStackTrace();
            try {
                conn.rollback();
            }catch (SQLException ex){
                ex.printStackTrace();
            }
        }
    }


    @Override
    public void emprunter(Client client, FilmDTO film) {
        emprunter(client.getCodeClient(), film.getNfilm());
    }

    @Override
    public void restituer(int nclient, int nfilm) {
        Connection conn = ConnectCinema.getInstance();
        try {
            PreparedStatement ps = conn.prepareStatement(SQLRestituer);

            ps.setInt(1, nclient);
            ps.setInt(2, nfilm);

            conn.setAutoCommit(false);
            ps.executeUpdate();
            conn.commit();
        }catch (SQLException e){
            e.printStackTrace();
            try {
                conn.rollback();
            }catch (SQLException ex){
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void restituer(Client client, FilmDTO film) {
        restituer(client.getCodeClient(), film.getNfilm());
    }

    public static void main(String[] args) {
        DAOMetier dao = new DAOMetier();
      /*  System.out.println("-------------------liste genre-------------------");
        System.out.println(dao.ensGenres());
        System.out.println("---------------nombre de film-------------------------");
        System.out.println("Categorie 2 a : " + dao.nbreFilmDuGenre(2) + " film(s)");
        System.out.println("----------------------liste film du genre-----------------------");
        System.out.println(dao.ensFilmsDuGenre(2));
        System.out.println("--------------------------info real acteur----------------------------");
        FilmDTO f = dao.infoRealisateurEtActeur(3);
        System.out.println(f.getTitre() +"\n"
                            +   f.getNomRealisateur() + "\n"
                            +   f.getNomActeur());*/

        /*dao.createClient("biffe", "pierre", "valenciennes",2);
        dao.createClient("glandu", "benjamin", "monchaux",1);
        dao.createClient("glandu", "arthur", "lille",1);
        dao.createClient("pif", "herclue", "valenciennes",0);
        dao.createClient("leuleu", "jean-marie", "ronchin",3);*/

        /*for (Client client : dao.findAllClients()){
            System.out.println(client);
        }*/

        //dao.emprunter(3,7);
        //dao.emprunter(3,5);

        //dao.restituer(3,7);
        //dao.deleteClient(2);
        //dao.deleteClient(3);


    }
}
