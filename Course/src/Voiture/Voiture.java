package Voiture;

public class Voiture {
    //variable d'instance
    private int imma;
    private String marque;

    //association
    public Personne conducteur=null; //je suis libre

    //variable static
    private static int derniereImma =1;

    //composition
    private Moteur moteur = new Moteur();

    //getter et setter
    public int getImma() {

        return imma;
    }

    public void setImma() {
        this.imma = Voiture.getDerniereImma();
        Voiture.setDerniereImma();
    }

    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = (marque==null || marque.trim().equals(("")) ? "INCONNU" : marque.toUpperCase());
    }

    public static int getDerniereImma() {
        return derniereImma;
    }

    private static void setDerniereImma() {
        Voiture.derniereImma++;
    }

    // metier
    public boolean estDisponible(){
        return (this.conducteur == null);
    }

    public Personne getConducteur() {
        return conducteur;
    }

    public void setConducteur(Personne conducteur) {
        this.conducteur = conducteur;
    }

    //constructeur


    public Voiture(String marque, int puissance, char carburant) {
        this.setImma();
        this.setMarque(marque);
        this.moteur = new Moteur(puissance, carburant);
    }

    public Voiture() {
        this(null, 1000, 'E');
    }

    public Voiture(String marque, Moteur moteur) {
        if(moteur!=null)
            this.moteur = new Moteur(moteur.getPuissance(),moteur.getCarburant());
        this.setImma();
        this.setMarque(marque);

    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName()+"{" +
                "imma=" + imma +
                ", marque='" + marque + '\'' +
                ", moteur=" + moteur +
                "est piloté par : " + (conducteur==null ? "personne" : conducteur.getNom()) +
                '}';
    }

    //test unitaire
    public static void main(String[] args) {
        Voiture v1 = new Voiture("RENAULT", 1500, 'E');
        Voiture v2 = new Voiture();
        Moteur m1 = new Moteur(2000, 'E');
        Voiture v3 = new Voiture("PORSCHE",m1);
        System.out.println(v1);
        System.out.println(v2);
        System.out.println(m1);
        System.out.println(v3);
    }

}
