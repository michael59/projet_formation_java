<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: michael
  Date: 25/02/2020
  Time: 13:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Carriere d'un acteur</title>
</head>
<body>
Carrière de ${acteur.prenom} ${acteur.nom}
<br>
<table>
    <thead>
    <tr>

        <th>Réalisateur</th>
        <th>titre</th>
        <th>nature</th>

    </tr>
    </thead>
    <tbody>
    <c:forEach var="film" items="${carriere}">
        <tr>
            <td>
                ${film.nomRealisateur}
            </td>
            <td>
                <a href="process?action=infofilm&nfilm=${film.nfilm}">
                    ${film.titre}
                </a>
            </td>
            <td>
                ${film.ngenre}
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
</body>
</html>
