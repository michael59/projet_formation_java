package modele;

import javax.persistence.*;
import java.sql.Date;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "ACTEUR", schema = "cinema", catalog = "")
public class ActeurEntity {
    private int nacteur;
    private String nom;
    private String prenom;
    private Date naissance;
    private Integer nbreFilms;
    private PaysEntity pays;
    private Collection<FilmEntity> films;

    @Id
    @Column(name = "nacteur")
    public int getNacteur() {
        return nacteur;
    }

    public void setNacteur(int nacteur) {
        this.nacteur = nacteur;
    }

    @Basic
    @Column(name = "nom")
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Basic
    @Column(name = "prenom")
    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    @Basic
    @Column(name = "naissance")
    public Date getNaissance() {
        return naissance;
    }

    public void setNaissance(Date naissance) {
        this.naissance = naissance;
    }

    @Basic
    @Column(name = "nbreFilms")
    public Integer getNbreFilms() {
        return nbreFilms;
    }

    public void setNbreFilms(Integer nbreFilms) {
        this.nbreFilms = nbreFilms;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ActeurEntity that = (ActeurEntity) o;
        return nacteur == that.nacteur &&
                Objects.equals(nom, that.nom) &&
                Objects.equals(prenom, that.prenom) &&
                Objects.equals(naissance, that.naissance) &&
                Objects.equals(nbreFilms, that.nbreFilms);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nacteur, nom, prenom, naissance, nbreFilms);
    }

    @ManyToOne
    @JoinColumn(name = "nationalite", referencedColumnName = "npays")
    public PaysEntity getPays() {
        return pays;
    }

    public void setPays(PaysEntity pays) {
        this.pays = pays;
    }

    @OneToMany(mappedBy = "acteurPrincipal")
    public Collection<FilmEntity> getFilms() {
        return films;
    }

    public void setFilms(Collection<FilmEntity> films) {
        this.films = films;
    }
}
